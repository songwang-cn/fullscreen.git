/* eslint-disable no-unused-vars */
/**
  * # 😡 FullScreen 类
  * @param containerClassName 容器类名
  * @param pageClassName 单页类名
  */

class FullScreen {
    fullContainer

    fullPageList

    pageNum

    bgColors

    page = 0

    timer = 0

    direction = 'vertical'

    constructor(containerClassName, pageClassName, direction, bgColors) {
        const fullPageList = Array.from(document.getElementsByClassName(pageClassName))
        // eslint-disable-next-line prefer-destructuring
        this.fullContainer = document.getElementsByClassName(containerClassName)[0]
        this.pageNum = fullPageList.length
        this.direction = direction || 'vertical'
        this.fullPageList = fullPageList
        this.bgColors = bgColors || []
        this.init()
    }

    init() {
        this.fullContainer.style.position = 'fixed'
        this.fullContainer.style.inset = '0'

        // eslint-disable-next-line array-callback-return
        this.fullPageList.map((item, i) => {
            if (this.direction === 'vertical') {
                item.style.height = i === 0 ? `${window.innerHeight}px` : '0px'
                item.style.transformOrigin = '50% 100%'
                item.style.width = '100%'
                item.style.left = '0'
                item.style.bottom = '0'
            } else if (this.direction === 'horizontal') {
                item.style.width = i === 0 ? `${window.innerWidth}px` : '0px'
                item.style.transformOrigin = '100% 50%'
                item.style.height = `${window.innerHeight}px`
                item.style.right = '0'
                item.style.bottom = '0'
            }
            item.style.backgroundColor = this.bgColors[i]
            item.style.boxShadow = '0 0 50px rgba(0, 0, 0, 0.3)'
            item.style.position = 'absolute'
            item.style.transition = 'all 500ms ease-in-out'
        })

        document.body.appendChild(this.fullContainer)

        document.addEventListener('wheel', (e) => {
            console.log(e)
            if (this.timer) return

            this.timer = 1

            setTimeout(() => {
                this.timer = 0
            }, 500)

            if (e.deltaY > 0) {
                this.page = this.page >= this.pageNum - 1 ? this.pageNum - 1 : this.page += 1
            } else {
                this.page = this.page <= 0 ? 0 : this.page -= 1
            }

            // eslint-disable-next-line array-callback-return
            this.fullPageList.map((item, index) => {
                if (this.page === index) {
                    if (this.direction === 'vertical') {
                        item.style.height = `${window.innerHeight}px`
                    } else if (this.direction === 'horizontal') {
                        item.style.width = `${window.innerWidth}px`
                    }
                    item.style.opacity = '1'
                } else {
                    if (this.direction === 'vertical') {
                        item.style.height = '0px'
                    } else if (this.direction === 'horizontal') {
                        item.style.width = '0px'
                    }
                    item.style.opacity = '0'
                }
            })
        })
    }

    /**
    * # 😡 初始化静态方法
    * @example FullScreen.init(containerClassName, pageClassName)
    * @param option FullScreenOptions
    */

    static init(option) {
        new FullScreen(option.el, option.pageClassName, option.direction, option.bgColors).init()
    }
}
