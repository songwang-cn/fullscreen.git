# fullscreen

## 示例地址



#### 介绍
轻量级全屏插件，支持全屏页面的上下滚动，左右滚动


#### 安装教程

npm install fullscreen

#### 使用说明

```
FullScreen.init({
    el: 'wrapper',          // 容器类名
    pageClassName: 'page',  // 单页类名
    direction: 'vertical',  // 方向   // horizontal
    bgColors: [             // 单页背景色
        '#0a69f8', 
        'brown', 
        'lightblue'
    ],
})
```


